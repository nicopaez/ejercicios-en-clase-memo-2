# Bienvenido


[![pipeline status](https://gitlab.com/fiuba-memo2/modelo-de-repositorio/badges/master/pipeline.svg)](https://gitlab.com/fiuba-memo2/modelo-de-repositorio/commits/master)

## Instrucciones iniciales
Este el es repositorio que utilizaras durante la materia para realizar tus tareas.

Debes comenzar por modificar este archivo:

* Donde dice "Bienvenido" coloca tu nombre completo
* Ajusta la linea que refiere al "pipeline status" para que refleje el estado de tu proyecto en lugar del proyecto base
* Luego de los dos ajustes anteriores debes eliminar todas la lineas de las "Intrucciones iniciales"

¡Suerte!
